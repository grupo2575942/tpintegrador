using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguimientoEnemigo : MonoBehaviour
{
    public float rangoAlerta;

    public LayerMask capaJugador;
    private GameObject jugador;

    bool alerta;

    public float velocidad;
    void Start()
    {
        jugador = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        alerta = Physics.CheckSphere(transform.position, rangoAlerta, capaJugador);

        if (alerta == true)
        {
            transform.LookAt(new Vector3(jugador.transform.position.x, transform.position.y, jugador.transform.position.z));
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(jugador.transform.position.x, transform.position.y, jugador.transform.position.z), velocidad * Time.deltaTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, rangoAlerta);
    }
}
