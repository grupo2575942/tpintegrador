using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJuego : MonoBehaviour
{
    float tiempo;

    public Text textoTiempo;
    public Text textoGameover;
    void Start()
    {
        Comenzarcronometro();
        setearTextos();
    }
    
    private void setearTextos()
    {
        textoTiempo.text = "Tiempo: " + tiempo.ToString();
    }

    public void Comenzarcronometro()
    {
        StartCoroutine(ComenzarCronometro(0));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 0)
    {
        tiempo = valorCronometro;
        while(true)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo++;
        }
    }
 
    void Update()
    {
        setearTextos();
        if(Input.GetKeyDown(KeyCode.R)) 
        {
            SceneManager.LoadScene("Scene01");
        }
    }
}
