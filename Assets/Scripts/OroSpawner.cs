using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class OroSpawner : MonoBehaviour
{

    public GameObject Oro;
    private int LimiteXZ = 35;
    void Start()
    {

        InvokeRepeating("Spawn", 0f, 5f);

    }

    
    void Update()
    {
        
    }

    void Spawn()
    {
        float PosicionX = Random.Range(-LimiteXZ, LimiteXZ);
        float PosicionZ = Random.Range(-LimiteXZ, LimiteXZ);

        Vector3 randomSpawn = new Vector3(PosicionX, 0.9f, PosicionZ);

        Instantiate(Oro, randomSpawn, Oro.transform.rotation);

    }
}
