using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemiges2 : MonoBehaviour
{
    public GameObject Enemigo;
    private int LimiteXZ = 35;

    void Start()
    {

        InvokeRepeating("Spawner", 10f, 10f);

    }


    void Update()
    {

    }

    void Spawner()
    {
        float PosicionX = Random.Range(-LimiteXZ, LimiteXZ);
        float PosicionZ = Random.Range(-LimiteXZ, LimiteXZ);

        Vector3 randomSpawn = new Vector3(PosicionX, 0.9f, PosicionZ);

        Instantiate(Enemigo, randomSpawn, Enemigo.transform.rotation);

    }
}
