using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo2 : MonoBehaviour
{
    public GameObject proyectilAzul;

    public Transform spawnBala;

    public float rapidez = 50;
    public float frecuencia = 0.5f;
    private float frecuenciaDisparo = 0;
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (Time.time > frecuenciaDisparo)
            {
                GameObject newBullet;

                newBullet = Instantiate(proyectilAzul, spawnBala.position, spawnBala.rotation);

                Rigidbody rb = newBullet.GetComponent<Rigidbody>();
                rb.AddForce(spawnBala.forward * rapidez, ForceMode.Impulse);

                GestorDeAudio.instancia.ReproducirSonido("Disparo");

                frecuenciaDisparo = Time.time + frecuencia;

                Destroy(newBullet, 5);
            }
        }
    }
}
