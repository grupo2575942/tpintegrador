using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigo : MonoBehaviour
{
    bool alerta;

    public float rangoAlerta;
    public LayerMask capaJugador;

    public GameObject proyectil;

    public Transform spawnBala;

    private GameObject player;

    public float velBala = 25;
    public float frecuencia = 2.5f;
    private float frecuenciaDisparo = 0;
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    
    void Update()
    {
        alerta = Physics.CheckSphere(transform.position, rangoAlerta, capaJugador);

        if (alerta == true && Time.time > frecuenciaDisparo)
        {
            Disparo();
            
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, rangoAlerta);
    }

    void Disparo()
    {
        Vector3 direccionPlayer = player.transform.position - transform.position;

        GameObject newBala;

        newBala = Instantiate(proyectil, spawnBala.position, spawnBala.rotation);
        newBala.GetComponent<Rigidbody>().AddForce(direccionPlayer * velBala, ForceMode.Force);

        frecuenciaDisparo = Time.time + frecuencia;
    }
}
