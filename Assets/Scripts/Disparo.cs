using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    
    public GameObject proyectilRojo;

    

    public Transform spawnBala;

    public float rapidez;
    public float frecuencia ;
    private float frecuenciaDisparo = 0;

    void Start()
    {
        
    }


    void Update()
    {
        
        if (Input.GetMouseButton(0))
        {
            if(Time.time > frecuenciaDisparo)
            {
                GameObject newBullet;

                newBullet = Instantiate(proyectilRojo, spawnBala.position, spawnBala.rotation);

                Rigidbody rb = newBullet.GetComponent<Rigidbody>();
                rb.AddForce(spawnBala.forward * rapidez, ForceMode.Impulse);

                GestorDeAudio.instancia.ReproducirSonido("Disparo");

                frecuenciaDisparo = Time.time + frecuencia;
                
                Destroy(newBullet, 5);
            }
        }
    }
}
