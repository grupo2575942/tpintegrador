using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaEnemigoRojo : MonoBehaviour
{
    private int hp;

    public GameObject explosion;
    void Start()
    {
        hp = 100;
    }


    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectilRojo"))
        {
            recibirDaņo();
        }
    }

    public void recibirDaņo()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            GestorDeAudio.instancia.ReproducirSonido("Explosion");
            this.desaparecer();
            MostrarExplosion();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    public void MostrarExplosion()
    {
        GameObject particulas = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
    }
}
