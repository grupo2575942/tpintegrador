using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CambioArma : MonoBehaviour
{
    public Disparo[] armas;
    private int indiceArmaActual;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RevisarCambioDeArmas();
    }

    void CambiarArmaActual()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        armas[indiceArmaActual].gameObject.SetActive(true);
    }

    void RevisarCambioDeArmas()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SeleccionarArma1();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SeleccionarArma2();
        }
    }

    void SeleccionarArma1()
    {
        if (indiceArmaActual == 0)
        {
            indiceArmaActual = armas.Length - 1;
        }
        else
        {
            indiceArmaActual--;
        }
        CambiarArmaActual();
    }

    void SeleccionarArma2()
    {
        if (indiceArmaActual >= (armas.Length - 1))
        {
            indiceArmaActual = 0;
        }
        else
        {
            indiceArmaActual++;
        }
        CambiarArmaActual();
    }
}
