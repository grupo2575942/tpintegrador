using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;

    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;

    public Text textoCantidadRecolectados;
    public Text textoGanaste;

    private int cont;

    private int saltosRestantes;
    private int saltosMaximos;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 20;
        setearTextos();
        saltosMaximos = 1;
        textoGanaste.text = "";
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Puntos restantes: " + cont.ToString();

        if (cont == 0)
        {
            textoGanaste.text = "Ganaste";
            Time.timeScale = 0f;
        }

    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if(Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }



        if (Input.GetKeyDown(KeyCode.Space) && saltosRestantes > 0)
        {
            saltosRestantes = saltosRestantes - 1;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse); 
        } 

        if (EstaEnPiso())
        {
            saltosRestantes = saltosMaximos;
        }
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Oro") == true)
        {
            cont = cont - 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Vacio") == true)
        {
            SceneManager.LoadScene("Scene01");
        }

        if ( other.gameObject.CompareTag("Enemigo") == true)
        {
            SceneManager.LoadScene("Scene01");
        }
    }
    public bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
}
